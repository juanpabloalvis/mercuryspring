create DATABASE springapp;

grant all on springapp.* TO springappuser@'%' IDENTIFIED BY 'pspringappuser';
grant all on springapp.* TO springappuser@localhost IDENTIFIED BY 'pspringappuser';

USE springapp;

create TABLE springapp.products (
  id INTEGER PRIMARY KEY,
  name varchar(255),
  price decimal(15,2)
);
create INDEX products_description ON products(name);


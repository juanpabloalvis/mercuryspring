package com.companyname.springapp.repository;

import com.companyname.springapp.domain.Product;

import java.util.List;

public interface ProductDao {

    List<Product> getProductList();

    void saveProduct(Product prod);

}
package com.companyname.springapp.mapper;

import com.companyname.springapp.entity.SysUserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author GaoJun.Zhou
 * @since 2017-06-30
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
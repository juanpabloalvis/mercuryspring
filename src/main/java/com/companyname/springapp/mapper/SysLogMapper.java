package com.companyname.springapp.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.companyname.springapp.entity.SysLog;


/**
 * <p>
  * 日志表 Mapper 接口
 * </p>
 *
 * @author GaoJun.Zhou
 * @since 2017-07-06
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
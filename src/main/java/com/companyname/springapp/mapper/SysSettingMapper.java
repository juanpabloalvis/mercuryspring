package com.companyname.springapp.mapper;

import com.companyname.springapp.entity.SysSetting;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 系统设置表 Mapper 接口
 * </p>
 *
 * @author GaoJun.Zhou
 * @since 2017-06-30
 */
public interface SysSettingMapper extends BaseMapper<SysSetting> {

}
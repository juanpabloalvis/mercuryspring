package com.companyname.springapp.controller;

import com.companyname.springapp.domain.Product;
import com.companyname.springapp.service.ProductManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("api")
public class ProductsController {

    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private ProductManager productManager;


    @RequestMapping(value = "/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllProductsJson(HttpServletRequest request, HttpServletResponse response) {

        logger.info("Inside getAllProducts() method...");

        List<Product> allProducts = new ArrayList<Product>();
        Product product = new Product();
        product.setId(1);
        product.setPrice(1000.0);
        product.setName("Testing");
        Product product2 = new Product();
        product2.setId(2);
        product2.setPrice(2000.0);
        product2.setName("Testing two");
        allProducts.add(product);
        allProducts.add(product2);
        return new ResponseEntity<>(allProducts, HttpStatus.OK);

    }


    public void setProductManager(ProductManager productManager) {
        this.productManager = productManager;
    }
}
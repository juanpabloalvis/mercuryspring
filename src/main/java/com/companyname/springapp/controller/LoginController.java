package com.companyname.springapp.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;


/**
 * @author jameszhou
 */
@Controller
public class LoginController extends AdminController {

    /**
     * @return
     */
    @GetMapping(value = "login")
    public String login() {
        return "login";
    }

    /**
     * @return
     */
    //@RequestMapping(value = "/dologin", method = RequestMethod.POST)
    @PostMapping("/login")
    public String doLogin(String username, String password, RedirectAttributesModelMap model) {

        // https://github.com/telzhou618/extjs4-shiro-admin
        // credenciales: admin/123456
        // Eliminamos apache shiro, deberíamos utilizar spring-security creo que hay conflicto con spring initializar		
        
        
        return "redirect:/";
    }
}

package com.companyname.springapp.service;

import org.springframework.stereotype.Service;

@Service
public class ConstructorGreetingService implements GreetingService {
    
    public String sayGreeting() {
        return "Hola que tal! - Constructor";
    }
}

package com.companyname.springapp.service;

public interface GreetingService {
    String sayGreeting();
}

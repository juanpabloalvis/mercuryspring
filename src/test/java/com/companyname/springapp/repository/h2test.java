package com.companyname.springapp.repository;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class h2test {

    @Test
    public void dd() {
        try {
            DriverManager.registerDriver(new org.h2.Driver());

            Connection c = DriverManager.getConnection("jdbc:h2:mem:test");
            PreparedStatement stmt = c.prepareStatement("create TABLE PRODUCTS (\n" +
                    "-- create TABLE springapp.products (\n" +
                    "  id INTEGER PRIMARY KEY,\n" +
                    "  name varchar(255),\n" +
                    "  price decimal(15,2)\n" +
                    ");");
            stmt.execute();
            stmt.close();
            c.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package com.companyname.springapp.controller;

import com.companyname.springapp.service.ConstructorGreetingService;
import org.junit.Before;
import org.junit.Test;

public class ConstructorInjectedControllerTest {
    ConstructorInjectedController controller;
    @Before
    public void setUp()  {
        controller = new ConstructorInjectedController(new ConstructorGreetingService());
    }

    @Test
    public void getGreeting() {
        System.out.println(controller.getGreeting());
    }
}
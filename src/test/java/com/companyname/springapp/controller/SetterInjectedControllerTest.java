package com.companyname.springapp.controller;

import com.companyname.springapp.service.SetterInjectedGreetingService;
import org.junit.Before;
import org.junit.Test;

public class SetterInjectedControllerTest {

    SetterInjectedController controller;

    @Before
    public void setUp() {
        controller = new SetterInjectedController();
        controller.setGreetingService(new SetterInjectedGreetingService());
    }

    @Test
    public void getGreeting() {
        System.out.println(controller.getGretting());
        
    }
}
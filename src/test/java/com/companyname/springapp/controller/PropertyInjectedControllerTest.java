package com.companyname.springapp.controller;

import com.companyname.springapp.service.PropertyInjectedGreetingService;
import org.junit.Before;
import org.junit.Test;

public class PropertyInjectedControllerTest {
    PropertyInjectedController controller;

    @Before
    public void setUp() {
        controller = new PropertyInjectedController();
        controller.greetingService = new PropertyInjectedGreetingService();
    }

    @Test
    public void getGreeting() {
        System.out.println(controller.getGreeting());
    }
}
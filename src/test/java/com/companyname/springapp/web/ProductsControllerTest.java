package com.companyname.springapp.web;

import com.companyname.springapp.controller.ProductsController;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertNotNull;

public class ProductsControllerTest {


    @Test
    public void testGetAllProductsJson() throws Exception {
        ProductsController controller = new ProductsController();
        ResponseEntity<?> allProductsJson = controller.getAllProductsJson(null, null);
        allProductsJson.toString();


        assertNotNull(allProductsJson);
    }
}
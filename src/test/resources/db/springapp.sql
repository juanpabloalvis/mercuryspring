
create TABLE IF NOT EXISTS products (
  id INTEGER PRIMARY KEY,
  name varchar(255),
  price decimal(15,2)
);
--create INDEX products_description ON products(name);

